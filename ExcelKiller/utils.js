const ExcelJS = require('exceljs');
const { Sequelize , QueryTypes} = require('sequelize');
var path = require('path');


async function generate(excel_id, res){
    const workbook = new ExcelJS.Workbook()
    const workbookMaster = await workbook.xlsx.readFile('Master.xlsx')
    const sequelize = new Sequelize('angkasatool', 'root', '', {
        host: 'localhost',
        dialect: 'mysql'
    });

    const dataExcel = await sequelize.query("SELECT name FROM `excels` where id=" + excel_id + " LIMIT 1", { type: QueryTypes.SELECT });
    var excelName = dataExcel[0]['name'];
    

    // YOUTUBE BAGIAN
    const worksheetYoutube = workbookMaster.getWorksheet('YOUTUBE')
    const dataYoutube = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Youtube' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
    var accountYoutube = "";   
    dataYoutube.forEach(function (item,i){
        var row = 4+i;
        worksheetYoutube.getCell('D'+row).value = item['link'];
        if (accountYoutube != item['account']) {
            worksheetYoutube.getCell('C'+row).value = item['account'];
        }
        accountYoutube = item['account']
    });


    // FACEBOOK BAGIAN
    const worksheetFacebook = workbookMaster.getWorksheet('FACEBOOK')
    const dataFacebook = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Facebook' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
    var accountFacebook = "";   
    dataFacebook.forEach(function (item,i){
        var row = 4+i;
        worksheetFacebook.getCell('D'+row).value = item['link'];
        if (accountFacebook != item['account']) {
            worksheetFacebook.getCell('C'+row).value = item['account'];
        }
        accountFacebook = item['account']
    });

     // INSTAGRAM BAGIAN
     const worksheetInstagram = workbookMaster.getWorksheet('INSTAGRAM')
     const dataInstagram = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Instagram' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
     var accountInstagram = "";   
     dataInstagram.forEach(function (item,i){
         var row = 4+i;
         worksheetInstagram.getCell('D'+row).value = item['link'];
         if (accountInstagram != item['account']) {
             worksheetInstagram.getCell('C'+row).value = item['account'];
         }
         accountInstagram = item['account']
     });

    // TWITTER BAGIAN
    const worksheetTwitter = workbookMaster.getWorksheet('TWITTER')
    const dataTwitter = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Twitter' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
    var accountTwitter = "";   
    dataTwitter.forEach(function (item,i){
        var row = 4+i;
        worksheetTwitter.getCell('D'+row).value = item['link'];
        if (accountTwitter != item['account']) {
            worksheetTwitter.getCell('C'+row).value = item['account'];
        }
        accountTwitter = item['account']
    });

    // TIKTOK BAGIAN
    const worksheetTiktok = workbookMaster.getWorksheet('TIKTOK')
    const dataTiktok = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Tiktok' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
    var accountTiktok = "";   
    dataTiktok.forEach(function (item,i){
        var row = 4+i;
        worksheetTiktok.getCell('D'+row).value = item['link'];
        if (accountTiktok != item['account']) {
            worksheetTiktok.getCell('C'+row).value = item['account'];
        }
        accountTiktok = item['account']
    });

    // SNACKVIDEO BAGIAN
    const worksheetSnackvideo = workbookMaster.getWorksheet('SNACKVIDEO')
    const dataSnackvideo = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Snackvideo' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
    var accountSnackvideo = "";   
    dataSnackvideo.forEach(function (item,i){
        var row = 4+i;
        worksheetSnackvideo.getCell('D'+row).value = item['link'];
        if (accountSnackvideo != item['account']) {
            worksheetSnackvideo.getCell('C'+row).value = item['account'];
        }
        accountSnackvideo = item['account']
    });

    // wRITE TO FIle Fucker
    await workbookMaster.xlsx.writeFile('files/'+ excelName + '.xlsx');
}

generate(1)
console.log("done")