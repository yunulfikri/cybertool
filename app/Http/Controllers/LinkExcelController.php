<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Inertia\Inertia;
use App\Models\LinkExcel;
use App\Models\Excel;
use Illuminate\Support\Facades\DB;

class LinkExcelController extends Controller
{
    //
    public function index($id)
    {
        # code...
        $subtask = Excel::find($id);
        $user = Auth::user();

        //select distinct(links.user_id),users.name from links inner join users on users.id = links.user_id where subtask_id = 10;
        // $userVirall = LinkExcel::distinct('link_excels.user_id')->select('users.id','users.name')
        //             ->where('excel_id', $id)
        //             ->join('users','link_excels.user_id','=','users.id')->get();
        $getChart = LinkExcel::select(DB::raw('count(link_excels.link) as user_count, users.name'))->where('excels.id',$id)->where('excels.deleted_at', Null)
        ->join('users', 'users.id', '=', 'link_excels.user_id')
        ->join('excels', 'excels.id', '=', 'link_excels.excel_id')
        ->groupBy('link_excels.user_id')->get();
        $ownLink = LinkExcel::where('user_id',$user->id)
        ->where('excel_id', $id)
        ->orderBy('account')->get();
        // return $ownLink;
        return Inertia::render('ExcelLink', [
            'subtask' => $subtask,
            'datauser' => $user,
            'getchart' => $getChart,
            'ownlink' => $ownLink,
            'links' => LinkExcel::select('link_excels.*','users.name as operator')->where('link_excels.excel_id', $id)->join('users', 'users.id','=', 'link_excels.user_id')->get()
        ]);
    }
    public function store(Request $request){
        
        $user = Auth::user();
        $data = $request->links;
        $partial= explode("\n", $data);
        foreach ($partial as $value) {
            # check linkk kosong atau gak
            if ($value != "") {
                # code... masuk ke database
                LinkExcel::create([
                    'user_id' => $user->id,
                    'excel_id' => $request->excel_id,
                    'account' => $request->akun,
                    'link' => $value,
                    'sosialmedia' => $request->sosmed
                ]);
            }
        }
        return back();
        
    }
    public function destroy(Request $request)
    {
        # code...
        LinkExcel::destroy($request->id);
        return back();

    }
}
