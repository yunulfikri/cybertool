<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Task;
use App\Models\SubTask;
use App\Models\Link;
use Auth;

class LinkController extends Controller
{
    //
    public function index($id)
    {
        # code...
        $subtask = SubTask::find($id);
        $user = Auth::user();

        //select distinct(links.user_id),users.name from links inner join users on users.id = links.user_id where subtask_id = 10;
        $userVirall = Link::distinct('links.user_id')->select('users.id','users.name')
                    ->where('subtask_id', $id)
                    ->join('users','links.user_id','=','users.id')->get();
        return Inertia::render('Link', [
            'subtask' => $subtask,
            'datauser' => $user,
            'uservirall' => $userVirall,
            'links' => Link::select('links.*','users.name as operator')->where('links.subtask_id', $id)->join('users', 'users.id','=', 'links.user_id')->get()
        ]);
    }
    public function store(Request $request)
    {
        # code... 
        Link::create([
            'subtask_id' => $request->subtask_id,
            'link' => $request->link,
            'sosialmedia' => $request->sosialmedia,
            'user_id' => $request->user_id
        ]);
     
        return back();
        
    }
    public function destroy(Request $request)
    {
        # code...
        $user = Auth::user();
        Link::where('subtask_id', $request->subtask_id)->where('user_id', $user->id)->delete();


        $data = SubTask::find($request->subtask_id);

        $facebook = $data['facebook'] - $request->facebook;
        $instagram= $data['instagram'] - $request->instagram;
        $twitter= $data['twitter'] - $request->twitter;
        $tiktok= $data['tiktok'] - $request->tiktok;
        $youtube= $data['youtube'] - $request->youtube;
        $snackvideo= $data['snackvideo'] - $request->snackvideo;

        SubTask::find($request->subtask_id)->update([
            'facebook' => $facebook,
            'instagram' => $instagram,
            'twitter' => $twitter,
            'tiktok' => $tiktok,
            'youtube' => $youtube,
            'snackvideo' => $snackvideo
        ]);
        return back();
    }
    
}
