<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Task;
use App\Models\SubTask;
use App\Models\Link;
use Illuminate\Support\Facades\DB;

class SubTaskController extends Controller
{
    //
    public function index($id)
    {
        # code...
        //SELECT COUNT(links.link), users.name FROM `links` 
        // INNER JOIN `users` ON users.id = links.user_id 
        // INNER JOIN sub_tasks ON sub_tasks.id = links.subtask_id 
        // INNER JOIN tasks ON tasks.id = sub_tasks.task_id 
        // WHERE tasks.id = 1 GROUP BY links.user_id; 

        $getChart = Link::select(DB::raw('count(links.link) as user_count, users.name'))->where('tasks.id',$id)->where('sub_tasks.deleted_at', Null)
        ->join('users', 'users.id', '=', 'links.user_id')
        ->join('sub_tasks', 'sub_tasks.id', '=', 'links.subtask_id')
        ->join('tasks', 'tasks.id', '=', 'sub_tasks.task_id')
        ->groupBy('links.user_id')->get();
        $taskdata = Task::find($id);
        $subtask = SubTask::where('task_id',$id)->get();
        return Inertia::render('SubTask', [
            'taskdata' => $taskdata,
            'subtask' => $subtask,
            'getchart' => $getChart,
        ]);

    }
    public function store(Request $request)
    {
        # code...
        SubTask::create([
            'name' => $request->task_input,
            'deadline' => $request->deadline,
            'task_id' => $request->task_id,
            'facebook' => 0,
            'instagram' => 0,
            'twitter' => 0,
            'tiktok' => 0,
            'youtube' => 0,
            'snackvideo' => 0
        ]);
        return back();
    }
    public function show($id)
    {
        # code...
        $subtask = SubTask::find($id);

        return Inertia::render('SubTaskLink', [
            'subtask' => $subtask,
            'links' => Link::where('subtask_id',$id)->groupBy('sosialmedia')->get()
        ]);
    }
    public function updateakun(Request $request)
    {
        # code...
        $data = SubTask::find($request->id);

        $facebook = $data['facebook'] + $request->facebook;
        $instagram= $data['instagram'] + $request->instagram;
        $twitter= $data['twitter'] + $request->twitter;
        $tiktok= $data['tiktok'] + $request->tiktok;
        $youtube= $data['youtube'] + $request->youtube;
        $snackvideo= $data['snackvideo'] + $request->snackvideo;

        SubTask::find($request->id)->update([
            'facebook' => $facebook,
            'instagram' => $instagram,
            'twitter' => $twitter,
            'tiktok' => $tiktok,
            'youtube' => $youtube,
            'snackvideo' => $snackvideo
        ]);
        
    }
    public function generate($id)
    {
        # code...
        $data = SubTask::find($id);
        $datalink = Link::where('subtask_id',$id)->get();

        return response()->json($datalink);
    }
    public function destroy(Request $request)
    {
        # code...
        SubTask::destroy($request->id);
        return 'sukses';
    }
    public function getchart($id)
    {
        # code...
        $getChart = Link::select(DB::raw('count(links.link) as user_count, users.name'))->where('tasks.id',$id)
                    ->join('users', 'users.id', '=', 'links.user_id')
                    ->join('sub_tasks', 'sub_tasks.id', '=', 'links.subtask_id')
                    ->join('tasks', 'tasks.id', '=', 'sub_tasks.task_id')
                    ->groupBy('links.user_id')->get();
                    return $getChart;
                    return response()->json($getChart);
    }
}
