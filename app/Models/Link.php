<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Link extends Model
{
    use HasFactory;
        use SoftDeletes;
    protected $fillable = ['subtask_id','link','sosialmedia','user_id'];

    // protected $table = 'sub_tasks';
}
