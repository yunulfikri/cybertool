<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LinkExcel extends Model
{
    use HasFactory;
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['user_id','excel_id','account','link','sosialmedia'];
}
