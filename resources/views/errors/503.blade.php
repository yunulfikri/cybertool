@extends('errors::minimal')

@section('title', __('Sedang Maintenance'))
@section('code', '503')
@section('message', __('Sedang Maintenance, Update Service! Mohon Tunggu Sebentar'))
